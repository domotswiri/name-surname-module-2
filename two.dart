/* 2.
* Create an array to store all the winning apps of the MTN Business 
* App of the Year Awards since 2012; 
* a) Sort and print the apps by name;  
* b) Print the winning app of 2017 and the winning app of 2018.; 
* c) the Print total number of apps from the array.
*/

void main() {
  final all_aps = <String>[];
  var winning_apps = Map();

  winning_apps["2012"] = <String>[
    'fnb_app',
    'discoverty_health_id',
    'matchy',
    'phrazapp',
    'plascon',
    'rapid-targets',
    'transunion-dealers-guide',
  ]; // MTN Business App of the Year Winning Apps for 2012;

  winning_apps['2013'] = <String>[
    'bookly',
    'deloitte-digital',
    'dstv',
    'gautrain-buddy',
    'kids-aid',
    'markitshare',
    'nedbank',
    'price-check',
    'price-check',
    'snapscan',
  ]; // MTN Business App of the Year Winning Apps for 2013;

  winning_apps['2014'] = <String>[
    'live-inspect',
    'my-belongings',
    'rea-vaya',
    'supersport',
    'sync-mobile',
    'vigo',
    'wildlife-tracker',
    'zapper',
  ]; // MTN Business App of the Year Winning Apps for 2014;

  winning_apps['2015'] = <String>[
    'cput-mobile',
    'dstv',
    'eskomsepush',
    'm4jam',
    'vula-mobile',
    'wumdrop',
  ]; // MTN Business App of the Year Winning Apps for 2015;

  winning_apps['2016'] = <String>[
    'domestly',
    'friendly-math-monsters-for-kindergarten',
    'hearza',
    'ikhokha',
    'kaching',
    'mibrand',
    'tuta-me',
  ]; // MTN Business App of the Year Winning Apps for 2016;

  winning_apps['2017'] = <String>[
    'orderin',
    'intergreatmev',
    'pick-n-pays-super-animals',
    'watif-health-portal',
    'transunion-1check',
    'hey-jude',
    'hey-jude',
    'oru-social-touchsa',
    'awethu-project',
    'zulzi',
    'shyft-for-standard-bank',
    'ecoslips',
  ]; // MTN Business App of the Year Winning Apps for 2017;

  winning_apps['2018'] = <String>[
    'cowa-bunga',
    'acgl',
    'besmarta',
    'xander-english',
    'ctrl',
    'pineapple',
    'bestee-1',
    'stokfella',
    'asi-snakes',
  ]; // MTN Business App of the Year Winning Apps for 2018;

  winning_apps['2019'] = <String>[
    'digger',
    'si-realities',
    'vula-mobile2',
    'hydra-farm',
    'matric-live',
    'franc',
    'over',
    'loctransi',
    'naked-insurance',
    'my-pregnancy-journey',
    'loot-defence',
    'mowash',
  ]; // MTN Business App of the Year Winning Apps for 2019;

  winning_apps['2020'] = <String>[
    'cherkers-sixty-60',
    'easy-equities',
    'birdpro',
    'technishen',
    'matric-live',
    'stokfella',
    'bottles',
    'lexie-hearing',
    'league-of-legends',
    'xitsonga-dictionary',
    'examsta',
    'xitsonga-dictionary',
    'greenfingers-mobile',
    'guardian-health',
    'my-pregnancy-journey',
  ]; // MTN Business App of the Year Winning Apps for 2020;

  winning_apps['2021'] = <String>[
    'Ambani-Afrika',
    'Takealot',
    'Shytft',
    'iiDENTIFii',
    'SISA',
    'Guardian-Health-Platform',
    'Murimi',
    'UniWise',
    'Kazi-App',
    'Rekindle',
    'Hellopay-SoftPOS',
    'Roadsave'
  ]; // MTN Business App of the Year Winning Apps for 2021;

  // Store all the winning apps of the
  // MTN Business App of the Year Awards since 2012
  winning_apps.forEach((k, v) => v.forEach((i) => all_aps.add(i)));

  // a) Sort and print the apps by name;
  all_aps.sort();
  print(all_aps);

  // b) Print the winning app of 2017 and the winning app of 2018.;

  // print("Winning app of 2017: ${winning_apps['2017']}");
  // print("Winning app of 2018: ${winning_apps['2018']}");

  print(winning_apps['2017']); // Winning app of 2017
  print(winning_apps['2018']); // Winning app of 2018

  // c) the Print total number of apps from the array.
  // print("Total number of apps: ${all_aps.length}");

  print(all_aps.length); // Total number of apps
}
