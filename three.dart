// Create a class and
// a) then use an object to print the name of the app, sector/category,
//    developer, and the year it won MTN Business App of the Year Awards.
// b) Create a function inside the class, transform the app name to all
//    capital letters and then print the output.

void main() {
  String appName = "Ambani Africa";
  String category = 'Education';
  String developer = 'Lambani';
  String yearWon = '2020';

  final app = new App(appName, category, developer, yearWon);

  app.appname();
}

class App {
  String appName;
  String category;
  String developer;
  String yearWon;

  App(this.appName, this.category, this.developer, this.yearWon);

  void appname() {
    return print(this.appName.toUpperCase());
  }
}
